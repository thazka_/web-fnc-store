<?php if (! defined('BASEPATH')) exit('No direct script access allowed');


	/**
	* class Admin
	*/
	class Admin extends CI_Controller
	{
		function __construct(){
			parent::__construct();
			// $this->load->library('encryption');
			// if ($this->session->userdata('username') == "") {
			// 	redirect('home');
			// }

			$this->load->helper('text');
		}
		
		function index()
		{
			// $data['username'] = $this->session->userdata('username');

			// $data['daftar_berita'] = $this->model_konten_info->view_all_berita()->result();
			// $data['daftar_pengumuman'] = $this->model_konten_info->view_all_pengumuman()->result();
			// $data['daftar_pesan'] = $this->model_pesan->view_all_pesan()->result();
			// $data['daftar_guru'] = $this->model_pengguna->view_all_guru()->result();
			// $data['daftar_siswa'] = $this->model_pengguna->view_all_siswa()->result();

			$this->load->view('admin/v_admin_header');
			$this->load->view('admin/v_admin_sidebar');
			$this->load->view('admin/v_admin_dashboard');
			$this->load->view('admin/v_admin_footer');
			
		}

		function tambah_customer()
		{

			$this->load->view('admin/v_admin_header');
			$this->load->view('admin/v_admin_sidebar');
			$this->load->view('admin/v_admin_customer_tambah');
			$this->load->view('admin/v_admin_footer');
			
		}

		function lihat_customer()
		{

			$this->load->view('admin/v_admin_header');
			$this->load->view('admin/v_admin_sidebar');
			$this->load->view('admin/v_admin_customer_lihat');
			$this->load->view('admin/v_admin_footer');
			
		}

		function perbaharui_customer()
		{

			$this->load->view('admin/v_admin_header');
			$this->load->view('admin/v_admin_sidebar');
			$this->load->view('admin/v_admin_customer_perbaharui');
			$this->load->view('admin/v_admin_footer');
			
		}
	}
 ?>